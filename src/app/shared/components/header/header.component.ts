import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core';
import { Router } from '@angular/router';
import { BaseRouter } from 'src/app/core/base/BaseRouter';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseRouter implements OnInit {
  username: string;
  constructor(public authenticationService: AuthenticationService, public router: Router) {
    super(router);
    this.username = authenticationService.getCredentials().UserName || null;
  }

  ngOnInit() {}

  validateSession() {
    if (this.authenticationService !== undefined) {
      return this.authenticationService.getCredentials() !== null;
    } else {
      return false;
    }
  }
}
