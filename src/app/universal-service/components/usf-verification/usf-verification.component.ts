import { Component, OnInit,ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import Util from 'src/app/universal-service/util';
import { UsfServiceService, ValidateSSNData } from 'src/app/core/usf/usf-service.service';
import { BaseComponent } from 'src/app/core/base/BaseComponent';
import { IonContent } from '@ionic/angular';
declare let $: any;
declare let alertify: any;

export interface Model {
  firstName: string;
  secondName: string;
  lastName: string;
  birthday: string;
  sufix: string;
  socialSecure: string;
}

@Component({
  selector: 'app-usf-verification',
  templateUrl: './usf-verification.component.html',
  styleUrls: ['./usf-verification.component.scss']
})
export class UsfVerificationComponent extends BaseComponent implements OnInit {
  @ViewChild(IonContent) ionContent: IonContent;
  datePicker_is_init = false;
  processValidationNLAD = false;

  format2 = 'XXX-XX-XXXX';
  valueSSN = '';
  checkSSN = false;
  invalidSSN = true;

  public sufixes = ['MR', 'MRS', 'ENG', 'ATTY', 'DR'];

  public form: FormGroup;
  model: Model = new class implements Model {
    sufix = '';
    socialSecure = '';
    birthday = '';
    firstName = '';
    lastName = '';
    secondName = '';
  }();

  validateSSNData: ValidateSSNData;
  miFormulario: FormGroup;
  constructor(
    public authenticationService: AuthenticationService,
    public usfServiceService: UsfServiceService,
    public router: Router,
    public fb: FormBuilder
  ) {
    super(authenticationService, usfServiceService, router, fb);
    //para adquirir el numero de caso
    this.validateSSNData = this.usfServiceService.getValidateSSNData();
    this.miFormulario = new FormGroup({
      
        'nombre1': new FormControl(),
        'nombre2': new FormControl(),
        'fecha': new FormControl(),
        'SSN': new FormControl()
    })
  }

  ngOnInit() {
    window.scroll(0, 0);

    this.form = this.fb.group({
      sufix: [null, Validators.compose([Validators.required])],
      firstName: [null, Validators.compose([Validators.required])],
      secondName: [
        null,
        Validators.compose([
          // Validators.required
        ])
      ],
      lastName: [null, Validators.compose([Validators.required])],
      socialSecure: [null, Validators.compose([Validators.required])],
      birthday: [null, Validators.compose([Validators.required])]
    });
  }

  goToDocumentDigitalization() {
    if (this.form.valid && this.model.socialSecure.length === 11) {
      this.processValidationNLAD = true;

      console.log(this.model);

      const datos = {
        method: 'subscriberVerificationMcapi',
        UserID: this.authenticationService.credentials.userid,
        caseID: this.validateSSNData.CASENUMBER,
        Lookup_Type: 2,
        response: 1,

        program: Number(sessionStorage.getItem('program')),
        people_live: Number(sessionStorage.getItem('people_live')),
        pan: Number(sessionStorage.getItem('pan')),

        depent_sufijo: this.model.sufix,
        depent_name: this.model.firstName,
        depent_mn: this.model.secondName,
        depent_last: this.model.lastName,
        depent_dob: this.formatDate(this.inFormat(this.model.birthday)),
        depent_ssn: String(this.valueSSN)
          .replace('-', '')
          .replace('-', '')
          .replace('-', '')
      };

      console.log(datos);

      setTimeout(() => {
        this.usfServiceService.doAction(datos).subscribe(resp => {
          this.processValidationNLAD = false;
          this.usfServiceService.setRequiredDocumentData(resp.body.required);
          console.log(resp);

          if (!resp.body.HasError) {
            // if (resp.body.message === 'Subscriber passed all validations and verifications') {
            //   this.router.navigate(['/universal-service/account-creation'], { replaceUrl: true });
            // } else {
            this.router.navigate(['/universal-service/document-digitalization'], { replaceUrl: true });
            // }
          } else {
            alertify.alert('Aviso', resp.body.ErrorDesc, function() {});
          }
        });
      }, 2000);
    }
  }
  validateName(x){
    let text = {
      'nombre1': this.model.firstName,
      'nombre2': this.model.secondName,
      'fecha': this.model.birthday,
      "ssn": this.model.socialSecure
    };
    let param = x;
    var texto;
    if(param <3){
       switch(param){
        case 1:
          texto =text.nombre1;
          break;
        case 2:
          texto = text.nombre2;
          break;
          }
      if (this.hasSpecialCharacter(texto).hasSpecial) {
          alert("No se admiten caracteres especiales ni numeros en este campo, por favor intente nuevamente");
          console.log("is no letter");
          switch(param){
            case 1:
              text.nombre1 = this.hasSpecialCharacter(texto).validtext;
              this.model.firstName =text.nombre1;
              break;
            case 2:
              text.nombre2 = this.hasSpecialCharacter(texto).validtext;
              this.model.secondName = text.nombre2;
              break;
          }
      console.log(text);
      this.miFormulario.setValue(text);
     
    } else {
      console.log("is letter");
    }
  }else{
    switch(param){
      case 3:
          texto =text.fecha;
          if (this.hasnumericCharacter(texto).hasSpecial) {
            alert("No se admiten caracteres especiales ni letras en este campo, por favor intente nuevamente");
            console.log("is not date");
            text.fecha = ""
            this.model.birthday = text.fecha;
            console.log(text);
            this.miFormulario.setValue(text)
         }
          break;
      case 4:
          texto = text.ssn;
          if (this.hasSocialCharacter(texto).hasSpecial) {
            alert("No se admiten caracteres especiales ni letras en este campo, por favor intente nuevamente");
            console.log("is not ssn");
            text.ssn = "";
            this.model.socialSecure= text.ssn;
            console.log(text);
            this.miFormulario.setValue(text)
         }
          break;
    }
    
  }
}
  
  hasSpecialCharacter(texto){
    var text = texto;
    var letras = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var especial ={
      espcialindex:0,
      hasSpecial: false,
      validtext:''
    }
    for(let i=0; i<text.length; i++){
        if (letras.indexOf(text.charAt(i),0)==-1){
            especial.espcialindex = i;
            especial.hasSpecial = true;
            if(especial.espcialindex > 0){
              especial.validtext = text.substring(0,especial.espcialindex);
              if(especial.validtext.length == 1){ especial.validtext = ""}
              console.log('el texto valido es:'+ especial.validtext);
              break;
            }
        }
    }
    return especial;
  }

 hasnumericCharacter(numero){
  var text = numero;
  var numeros = "1234567890/";
  var especial ={
    espcialindex:0,
    hasSpecial: false,
    validtext:''
  }
  
  for(let i=0; i<text.length; i++){
      if (numeros.indexOf(text.charAt(i),0)==-1){
          especial.espcialindex = i;
          especial.hasSpecial = true;
          if(especial.espcialindex > 0){
            especial.validtext = text.substring(0,especial.espcialindex);
            if(especial.validtext.length == 1){ especial.validtext = ""}
            console.log('el texto valido es:'+ especial.validtext);
            break;
          }
      }
  }
  return especial;
 }
 hasSocialCharacter(numero){
  var text = numero;
  var numeros = "1234567890-X";
  var especial ={
    espcialindex:0,
    hasSpecial: false,
    validtext:''
  }
  
  for(let i=0; i<text.length; i++){
      if (numeros.indexOf(text.charAt(i),0)==-1){
          especial.espcialindex = i;
          especial.hasSpecial = true;
          if(especial.espcialindex > 0){
            especial.validtext = text.substring(0,especial.espcialindex);
            if(especial.validtext.length == 1){ especial.validtext = ""}
            console.log('el texto valido es:'+ especial.validtext);
            break;
          }
      }
  }
  return especial;
 }
  validateData(){
    
    window.scroll(0, 0);//scroll 
     this.ionContent.scrollToTop(300); 
     var nombre = this.model.firstName;
     var second = this.model.secondName;
     var lastName = this.model.lastName;
     var birthday = this.model.birthday;
     var socialSecure =this.model.socialSecure;

     if(this.hasNumericCharacter(nombre)){
       alert("Nombre invalido, por favor corrija");
       return;
     }

     if(this.hasNumericCharacter(second)){
      alert("Segundo nombre invalido, por favor corrija");
      return;
    }

    if(this.hasNumericCharacter(lastName)){
      alert("Segundo nombre invalido, por favor corrija");
      return;
    }

    if(this.hasletrasCharacter(birthday)){
      alert(" verifique fecha de nacimiento, por favor corrija");
      return;
    }
    
   

    if(this.hasletrasCharacter(socialSecure)){
      alert("verifique numero de seguro social, por favor corrija");
      return;
    }
     this.goToDocumentDigitalization()
  }

  hasNumericCharacter(texto){
    var text = texto;
    var letras = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var hasSpecial = false;
    for(let i=0; i<text.length; i++){
        if (letras.indexOf(text.charAt(i),0)==-1){
            hasSpecial = true;
        }
    }
    return hasSpecial;
}

hasletrasCharacter(numero){
  var text = numero;
  var numeros = "1234567890/-";
  var hasSpecial = false;
  for(let i=0; i<text.length; i++){
      if (numeros.indexOf(text.charAt(i),0)==-1){
          hasSpecial = true;
      }
  }
  return hasSpecial;
}


  goToRegisterCase() {
    this.router.navigate(['/universal-service/register-case'], { replaceUrl: true });
  }

  formatInputSocialSecure(input: string) {
    this.model.socialSecure = this.formatInput(this.model.socialSecure, this.format2);
  }
  // NUEVA ESTRUCCTURA >>>>>>>>>>>>>>>>>>>
  // tslint:disable-next-line:member-ordering
  inFormat(cadena_fecha: string) {
    const temp_str = cadena_fecha.replace('/', '');
    const temp_str2 = temp_str.replace('/', '');
    console.log(temp_str2);
    return {
      year: temp_str2.substr(4, 4),
      day: temp_str2.substr(2, 2),
      month: temp_str2.substr(0, 2)
    };
  }
  // tslint:disable-next-line:member-ordering
  public activarDatepickerFechaN() {
    if (this.datePicker_is_init === false) {
      // tslint:disable-next-line:prefer-const
      let dia = String(new Date().getDate());
      // tslint:disable-next-line:radix
      if (parseInt(dia) < 10) {
        dia = '0' + dia;
      }
      // tslint:disable-next-line:prefer-const
      let mes = String(new Date().getMonth());
      // tslint:disable-next-line:radix
      if (parseInt(mes) < 10) {
        mes = '0' + mes;
      }
      $('#inputControl3').datepicker({
        dateFormat: 'mm/dd/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:-21',
        maxDate: '-21y',
        minDate: '-100y',
        defaultDate: '-22y',
        onSelect: (dateText: any) => {
          console.log(dateText + ' *onSelect');
          this.model.birthday = dateText;
        },
        onChangeMonthYear: function(year: any, month: any, datepicker: any) {
          // #CAMBIO APLICADO y Necesario ya que al seleccionar el mes y cambiar el a#o en los selects
          // # no Cambiaba el valor del input  Ahora si se esta aplocando el cambio
          console.log('onChangeMonthYear');
          if ($('#inputControl3').val().length === 10) {
            console.log('to :' + month + ' ' + $('#inputControl3').val().sub + ' ' + year);

            const new_date = new Date(
              month +
                '/' +
                $('#inputControl3')
                  .val()
                  .substr(3, 2) +
                '/' +
                year
            );

            $('#inputControl3').datepicker('setDate', new_date);
          }
        }
      });
      this.datePicker_is_init = true;
    }
    $('#inputControl3').datepicker('show');
  }
  // tslint:disable-next-line:member-ordering
  public formateadorFecha(entrada: string) {
    // NO se aceptara mas de 10 caracteres
    if (entrada.length > 10) {
      entrada = entrada.substr(0, 10);
    }
    // Limpiando especificando caracteres no permitidos
    const patron = /abcdefghijklmnopqrstuvwxyz/gi;
    const nuevoValor = '';
    entrada = entrada.replace(patron, nuevoValor);

    if (entrada.length > 2 && entrada.indexOf('/') !== 2) {
      entrada = entrada.replace('/', '');
      entrada = entrada.substr(0, 2) + '/' + entrada.substr(2, entrada.length);
    }

    if (entrada.length > 5 && entrada.indexOf('/', 5) !== 5) {
      // caso para el 2do Slash
      entrada = entrada.substr(0, 5) + '/' + entrada.substr(5, 4);
    }
    if (entrada.length >= 10) {
      console.log(this.inFormat(entrada));
    }
    return entrada;
  }

  // tslint:disable-next-line:member-ordering
  public ic_blur() {
    setTimeout(() => {
      console.log('#blur :' + this.model.birthday);
    }, 200);
  }

  // tslint:disable-next-line:member-ordering
  public ic_key_up() {
    this.model.birthday = this.formateadorFecha(this.model.birthday);
    console.log(this.model.birthday);
    console.log('ic_key_up');
  }
  onBlurSSN() {
    if (this.model.socialSecure !== undefined) {
      // tslint:disable-next-line: radix
      if (
        this.model.socialSecure.length === 11 &&
        // tslint:disable-next-line: radix
        (parseInt(
          String(this.valueSSN)
            .replace('-', '')
            .replace('-', '')
        ) >= 99999999 ||
          // tslint:disable-next-line: radix
          parseInt(
            String(this.valueSSN)
              .replace('-', '')
              .replace('-', '')
          ) >= 9999999)
      ) {
        // this.model.socialSecure = 'XXX-XX-' + this.valueSSN.substr(5, 4);

        // si tiene los 2 - guiones  ya esta validado
        let remplazo = String(this.valueSSN.replace('-', '').replace('-', '')).replace('-', '');
        remplazo = 'XXX' + '-' + 'XX' + '-' + String(this.valueSSN).substr(-4, 4);

        console.log(remplazo);
        this.model.socialSecure = remplazo;
        console.log('onBlurSSN If');
      } else {
        console.log(this.valueSSN);
        this.model.socialSecure = undefined;
        this.valueSSN = undefined;
        this.checkSSN = false;
        console.log('onBlurSSN else');
      }
    }
  }
  onFocusSSN() {
    if (this.model.socialSecure !== undefined) {
      // tslint:disable-next-line: radix
      if (
        this.model.socialSecure.length === 11 &&
        // tslint:disable-next-line: radix
        (parseInt(
          String(this.valueSSN)
            .replace('-', '')
            .replace('-', '')
        ) >= 99999999 ||
          // tslint:disable-next-line: radix
          parseInt(
            String(this.valueSSN)
              .replace('-', '')
              .replace('-', '')
          ) >= 9999999)
      ) {
        // si trae guiones se limpia
        this.valueSSN = String(this.valueSSN)
          .replace('-', '')
          .replace('-', '')
          .replace('-', '');
        console.log(this.valueSSN);

        // Restaurando el valor
        this.model.socialSecure =
          String(this.valueSSN).substr(0, 3) +
          '-' +
          String(this.valueSSN).substr(3, 2) +
          '-' +
          String(this.valueSSN).substr(5, 4);

        console.log('onFocusSSN If');
      } else {
        this.model.socialSecure = undefined;
        this.valueSSN = undefined;
        this.checkSSN = false;
      }
    }
  }

  public formatInput(input: string, format: string) {
    if (this.valueSSN !== undefined && input.length !== 11) {
      if (this.valueSSN.length > input.replace('-', '').replace('-', '').length) {
        //is erasing data
        console.log(this.valueSSN);
        this.model.socialSecure = undefined;
        this.valueSSN = undefined;
        this.checkSSN = false;
        return '';
      }
    }
    /* BETA */
    if (
      input !== undefined &&
      input.length === 11 && // tslint:disable-next-line: radix
      (parseInt(
        String(this.valueSSN)
          .replace('-', '')
          .replace('-', '')
      ) >= 99999999 ||
        // tslint:disable-next-line: radix
        parseInt(
          String(this.valueSSN)
            .replace('-', '')
            .replace('-', '')
        ) >= 9999999)
    ) {
      console.log('antes del cambio');
      console.log(input);
      console.log(this.valueSSN);
      // si tiene los 2 - guiones  ya esta validado
      this.valueSSN = String(input)
        .replace('-', '')
        .replace('-', '')
        .replace('-', '');
      this.valueSSN =
        String(this.valueSSN).substr(0, 3) +
        '-' +
        String(this.valueSSN).substr(3, 2) +
        '-' +
        String(this.valueSSN).substr(5, 4);
      console.log(format);
      console.log(this.format2);
      console.log(this.model.socialSecure);
      this.invalidSSN = false;
      this.checkSSN = true;
      console.log(this.form.controls['socialSecure'].valid);
      return this.valueSSN;
    }
    /* * * * * * * * * * * * * * * * * */

    // Almacenando valor real en variable temporal
    if (input !== undefined && input.length > 1 && String(input).substr(input.length - 1, 1) !== 'X') {
      // console.log(input.substr(input.length - 1, 1));
      this.valueSSN += String(input.substr(input.length - 1, 1));
    } else {
      if (input !== 'X' && input !== 'XXX-XX-XXXX') {
        this.valueSSN = input;
        // console.log(input);
      } else {
        console.log('comparacion para eliminar');
        console.log(input + ' input');
        console.log(this.valueSSN + ' valueSSN');
      }
    }

    // tslint:disable-next-line:radix
    if (
      input !== undefined &&
      input.length === 11 && // tslint:disable-next-line: radix
      (parseInt(
        String(this.valueSSN)
          .replace('-', '')
          .replace('-', '')
      ) >= 99999999 ||
        // tslint:disable-next-line: radix
        parseInt(
          String(this.valueSSN)
            .replace('-', '')
            .replace('-', '')
        ) >= 9999999)
    ) {
      console.log('invalido con ' + input.length);
      this.invalidSSN = true;
    }

    // Si llega a 11 Caracteres hay que hacer la validacion Real contra el servicio
    // ya que tiene el formato XXX-XX-XXXX  de 11 caracteres serian 9 digitos
    // parseInt( this.valueSSN ) > 99999999 [USADO PARA VALIDAR Y CUMPLIR LA CONDICION QUE SEA NUERICO Y DE 9 DIGITOS ]
    // tslint:disable-next-line:radix
    if (
      input !== undefined &&
      input.length === 11 && // tslint:disable-next-line: radix
      (parseInt(this.valueSSN.replace('-', '').replace('-', '')) >= 99999999 ||
        // tslint:disable-next-line: radix
        parseInt(
          String(this.valueSSN)
            .replace('-', '')
            .replace('-', '')
        ) >= 9999999)
    ) {
      console.log('invalidSSN f');
      this.invalidSSN = false;
      this.checkSSN = true;
      console.log(this.valueSSN);
    }

    if (format === this.format2) {
      console.log(input);
      console.log(format);
      console.log(this.format2);
      if (input !== undefined && input.length === 4) {
        if (input[input.length - 1] === '-') {
          return input.substr(0, input.length - 1) + input.substr(input.length - 1, input.length);
        } else {
          return input.substr(0, input.length - 1) + '-' + input.substr(input.length - 1, input.length);
        }
      }

      if (input !== undefined && input.length === 7) {
        console.log(input);
        if (input[input.length - 1] === '-') {
          return input.substr(0, input.length - 1) + input.substr(input.length - 1, input.length);
        } else {
          return input.substr(0, input.length - 1) + '-' + input.substr(input.length - 1, input.length);
        }
      }

      if (input !== undefined && input.length > 7) {
        return input;
      } else {
        return input;
      }
    }
    console.log('va a blanquear');
    return '';
  }
}
