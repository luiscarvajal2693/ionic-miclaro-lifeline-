import { AuthenticationService } from 'src/app/core';
import { UsfServiceService } from 'src/app/core/usf/usf-service.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import Util from 'src/app/universal-service/util';
import * as moment from 'moment';
import { BaseRouter } from 'src/app/core/base/BaseRouter';
declare let alertify: any;
declare let $: any;

export interface ValidateSSNData {
  data: ValidateSSNDataContent[];
  dataObject: ValidateSSNDataContent[];
  response: string;
  CASENUMBER: number;
}

export interface ValidateSSNDataContent {
  accountType: string;
  address: string;
  ban: string;
  lifelineActivationDate: string;
  name: string;
  ssn: string;
  subscriberNumber: string;
  phone1: string;
  CASENUMBER: string;
}

export interface DataObjectAddress {
  CASENUMBER: string;
  CUSTOMERADDRESS: string;
  CUSTOMERLASTNAME: string;
  CUSTOMERNAME: string;
  DOB: string;
  SSN: string;
  SUGGESTADDRESS: string;
}

export interface PeopleData {
  number: number;
  money: string;
}

export interface DataAgencyMoneySelection {
  agency: string;
  ldiRestriction: boolean;
  peopleDataSelectedNumber: number;
  peopleDataSelected: PeopleData;
  earningsValidation: boolean;
  lifelineProgramInscription: boolean;
  aceptationTerm: boolean;
}

export class BaseComponent extends BaseRouter {
  public form: FormGroup;
  validateSSNData: ValidateSSNData;
  dataObjectAddress: DataObjectAddress[];
  moment = moment;
  alertify = alertify;
  $ = $;

  constructor(
    public authenticationService: AuthenticationService,
    public usfServiceService: UsfServiceService,
    public router: Router,
    public fb: FormBuilder
  ) {
    super(router);
    authenticationService.validaSessionActiva();
    this.authenticationService.getCredentials().timeLogin = new Date();
    this.validateSSNData = this.usfServiceService.getValue('validateSSNData');
    this.dataObjectAddress = this.usfServiceService.getValue('dataObjectAddress');
  }

  public goToHome() {
    this.router.navigate(['/home'], { replaceUrl: true }).then();
  }

  public checkNumbersOnly = (event: any): boolean => Util.checkNumbersOnly(event);

  public checkCharactersOnly = (event: any): boolean => Util.checkCharactersOnly(event);

  getFormatDateCustom = (date: string) => this.moment(new Date(date)).format('MM/DD/YYYY');
}
