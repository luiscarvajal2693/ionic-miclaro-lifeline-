import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from 'src/environments/environment';
import { CoreModule } from 'src/app/core/index';

import { HomeModule } from './home/home.module';

import { LoginModule } from './login/login.module';
import { UniversalServiceModule } from './universal-service/universal-service.module';

import { UsfCaseModule } from './usf-case/usf-case.module';
import { Camera} from '@ionic-native/camera/ngx';
@NgModule({
  declarations: [AppComponent],

  entryComponents: [],

  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    CoreModule,
    HomeModule,
    LoginModule,
    UniversalServiceModule,
    UsfCaseModule,

    AppRoutingModule],

  providers: [
    StatusBar,
    SplashScreen,Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule {}
